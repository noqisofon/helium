#include "config.h"

#include "helium/types.h"

#include "helium/String.hxx"
#include "helium/Type.hxx"

#include "helium/Array.hxx"


namespace helium {


    class ArrayIterator;


    Array::Array()
        : capacity_(DEFAULT_CAPACITY)
        , size_(0)
        , contents_(NULL)
    {
        contents_ = new Object*[capacity_];
    }
    Array::Array(size_type capacity)
        : capacity_(capacity)
        , size_(0)
        , contents_(new Object*[capacity])
    {
    }
    Array::Array(const Array* const& other)
        : capacity_(other->capacity_)
        , size_(other->size_)
        , contents_(new Object*[other->size_])
    {
        assign( other );
    }
    Array::Array(const Array* const& other, index_type start)
        : capacity_(other->capacity_)
        , size_(other->size_)
        , contents_(new Object*[other->size_])
    {
        assign( other, start );
    }
    Array::Array(const Array* const& other, index_type start, index_type end)
        : capacity_(other->capacity_)
        , size_(other->size_)
        , contents_(new Object*[other->size_])
    {
        assign( other, start, end );
    }


    bool Array::equals(const Object* const& other) {
        if ( other->getTypeID() != getTypeID() ) {

            return false;
        }

        return Array::equals( dynamic_cast<const Array* const>( other ) );
    }
    bool Array::equals(const Array* const& other) {
        Iterator* this_it  = getIterator();
        Iterator* other_it = other->getIterator();

        size_t match_count = 0;

        while ( this_it->atEnd() ) {
            Object* left  = this_it->next();
            Object* right = other_it->next();

            if ( left->equals( right ) )
                ++ match_count;
        }
        this_it->release();
        other_it->release();

        return match_count == size();
    }


    Iterator* Array::getIterator() const {
        return new ArrayIterator( this );
    }


    bool Array::contains(const Object* const& match_item) const {
        bool      ret = false;
        Iterator* it  = getIterator();

        while ( it->atEnd() ) {
            if ( match_item->equals( it->next() ) ) {
                ret = true;

                break;
            }
        }
        it->release();

        return ret;
    }


    void Array::assign(const Array* const& other) {
        Iterator* it = other->getIterator();

        for ( index_type i = 0; it->atEnd(); ++ i ) {
            put( i, it->next() );
        }
        it->release();
    }
    void Array::assign(const Array* const& other, index_type start) {
        Iterator* it = other->getIterator();

        if ( start > 0 )
            it->skip( start );

        for ( index_type i = 0; it->atEnd(); ++ i ) {
            put( i, it->next() );
        }
        it->release();
    }
    void Array::assign(const Array* const& other, index_type start, index_type end) {
        Iterator* it = other->getIterator();

        if ( start > 0 )
            it->skip( start );

        for ( index_type i = 0; it->atEnd(); ++ i ) {
            if ( i >= end )
                break;

            put( i, it->next() );
        }
        it->release();
    }


    void Array::copyTo(Array* another, index_type start) const {
        if ( another->size_ <= start )
            return ;

        Iterator* it = getIterator();

        for ( index_type i = start; it->atEnd(); ++ i ) {
            another->put( i, it->next() );
        }
        it->release();
    }
    void Array::copyTo(Array* another, index_type start, index_type end) const {
        if ( another->size_ <= start )
            return ;
        if ( another->capacity_ <= end )
            return ;

        Iterator* it = getIterator();

        for ( index_type i = start; it->atEnd(); ++ i ) {
            if ( i >= end )
                break ;

            another->put( i, it->next() );
        }
        it->release();
    }


    void Array::finalize() {
        Iterator* it = getIterator();

        while ( it->atEnd() ) {
            it->next()->release();
        }
        it->release();

        delete [] contents_;
    }


}
