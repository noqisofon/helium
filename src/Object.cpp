#include "config.h"

#include <stddef.h>

#include "helium/types.h"

#include "helium/String.hxx"
#include "helium/Type.hxx"

#include "helium/Object.hxx"


namespace helium {


    Object::Object(bool in_dynamic)
        : retain_count_(0), in_dynamic_(in_dynamic) {
    }


    Object::~Object() {
        release();
    }


    bool Object::equals(const Object* const& other) const {
        return this == other;
    }


    Type* Object::getType() const {
        return NULL;
    }


    TypeID Object::getTypeID() const {
        return 0;
    }


    unsigned int Object::getHashCode() const {
        return 0;
    }


    String* Object::toString() const {
    }


    void Object::retain() {
        retain_count_ ++;
    }


    void Object::release() {
        if ( -- retain_count_ <= 0 ) {
            finalize();
        }
    }


}
