#ifndef helium_Array_hxx
#define helium_Array_hxx

#include <stddef.h>
#include <stdint.h>

#include "helium/Object.hxx"
#include "helium/Iterator.hxx"


namespace helium {


    /*!
     *
     */
    class Array : public Object {
     public:
        typedef    size_t          size_type;
        typedef    unsigned int    index_type;

     public:
        /*!
         *
         */
        Array();
        /*!
         *
         */
        Array(size_type capacity);
        /*!
         *
         */
        Array(const Array* const& other);
        /*!
         *
         */
        Array(const Array* const& other, index_type start);
        /*!
         *
         */
        Array(const Array* const& other, index_type start, index_type end);


        /*!
         *
         */
        virtual ~Array() {
        }

     public:
        /*!
         * 配列が固定サイズかどうかを示す値を返します。
         */
        virtual bool isFixedSize() const { return true; }


        /*!
         * 配列が読み取り専用かどうかを示す表す値を返します。
         */
        virtual bool isReadOnly() const { return false; }


        /*!
         * 配列へのアクセスが同期されているかどうかを示す値を返します。
         */
        virtual bool isSynchronized() const { return false; }


        /*!
         * 配列の要素の総数を表す 32 bit 整数を返します。
         */
        virtual size_t size() const { return size_; }


        /*!
         *
         */
        virtual bool equals(const Object* const& other);
        /*!
         *
         */
        bool equals(const Array* const& other);


        /*!
         *
         */
        Iterator* getIterator() const;


        /*!
         * レシーバの要素の中に指定された引数が含まれていれば真を返します。
         */
        bool contains(const Object* const& match_item) const;
        /*!
         * レシーバの要素の中に指定された引数が含まれていれば真を返します。
         */
        bool contains(const Object& match_item) const {
            return contains( &match_item );
        }


        /*!
         *
         */
        Object* at(index_type index) const {
            return contents_[index];
        }


        /*!
         *
         */
        void put(index_type index, Object* const& item) {
            contents_[index] = item;
        }


        /*!
         * レシーバの簡易コピーを作成して返します。
         */
        Array* clone() const {
            return new Array( this );
        }


        /*!
         *
         */
        void assign(const Array* const& other);
        /*!
         *
         */
        void assign(const Array* const& other, index_type start);
        /*!
         *
         */
        void assign(const Array* const& other, index_type start, index_type end);


        /*!
         * レシーバの要素をコピーした新しい配列を作成して返します。
         */
        Array* copy(index_type start = 0) const {
            return new Array( this, start );
        }
        /*!
         *
         */
        Array* copy(index_type start, index_type end) const {
            return new Array( this, start, end );
        }


        /*!
         * レシーバの要素全てを指定した配列にコピーします。
         */
        void copyTo(Array* another, index_type start = 0) const;
        /*!
         *
         */
        void copyTo(Array* another, index_type start, index_type end) const;


        /*!
         * pred で真が返された最初の要素を返します。
         */
        template <typename _Predicate>
        Object* find(_Predicate& pred) {
            Object*   ret = NULL;
            Iterator* it  = getIterator();

            while ( it->atEnd() ) {
                Object* current = it->next();

                if ( pred( current ) ) {
                    ret = current;

                    break ;
                }
            }
            it->release();

            return ret;
        }


        /*!
         * pred で真が返された全ての要素を返します。
         */
        template <typename _Predicate>
        Array* findAll(_Predicate& pred) {
            Array*     ret   = new Array( capacity_ );

            index_type index = 0;
            Iterator*  it    = getIterator();

            while ( it->atEnd() ) {
                Object* current = it->next();

                if ( pred( current ) )
                    ret->put( index ++, current );
            }
            it->release();

            return ret;
        }


     protected:
        /*!
         *
         */
        virtual void finalize();

     private:
        static const size_t DEFAULT_CAPACITY = 4;

     private:
        size_t capacity_;
        size_t size_;
        Object** contents_;
    };


}


#endif  /* helium_Array_hxx */
// Local Variables:
//   coding: utf-8
// End:
