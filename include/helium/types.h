#ifndef helium_types_h
#define helium_types_h

/* typedef    signed char           sint8_t; */
/* typedef    signed short          sint16_t; */
/* typedef    signed long           sint32_t; */
/* typedef    signed int            sint_t; */
/* typedef    signed long long      sint64_t; */

/* typedef    unsigned char         uint8_t; */
/* typedef    unsigned short        uint16_t; */
/* typedef    unsigned long         uint32_t; */
/* typedef    unsigned int          uint_t; */
/* typedef    unsigned long long    uint64_t; */

/* typedef    float                 float32_t; */
/* typedef    double                float64_t; */


typedef    unsigned int          TypeID;

#endif  /* helium_types_h */
// Local Variables:
//   coding: utf-8
// End:
