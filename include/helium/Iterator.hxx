/*
 * Iterator.hxx
 *
 *  Created on: 2014/01/05
 *      Author: rihine
 */
#ifndef helium_Iterator_hxx
#define helium_Iterator_hxx

#include "helium/Object.hxx"


namespace helium {


	/*!
	 *
	 */
	class Iterator : public Object {
     public:
		virtual bool atEnd() const = 0;
		virtual void skip(int skip_element = 1) = 0;
		virtual Object* next() = 0;
	};


}


#endif /* helium_Iterator_hxx */
