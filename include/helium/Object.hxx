#ifndef helium_Object_hxx
#define helium_Object_hxx


namespace helium {


    class String;
    class Type;


    /*!
     * \class Object Object.hxx
     * \brief helium の全てのクラスをサポートし、派生クラス二階レベルのサービスを提供します。
     */
    class Object
    {
     public:
        /*!
         * Object クラスの新しいインスタンスを初期化します。
         */
        explicit Object(bool in_heap = false);


        /*!
         * オブジェクトを破棄します。
         */
        virtual ~Object();

     public:
        /*!
         * \param left
         * \param right
         *
         * 指定された 2 つのオブジェクトが等しいかどうかを判断します。
         */
        static bool equals(const Object* const& left, const Object* const& right) {
            return left->equals( right );
        }


        /*!
         * \param left
         * \param right
         *
         * 指定した 2 つのオブジェクトが同一かどうかを判断します。
         */
        static bool referenceEquals(const Object* const& left, const Object* const& right) {
            return left == right;
        }

     public:
        /*!
         * \param other 
         *
         * 指定されたオブジェクトがオブジェクトと等しいかどうかを判断します。
         */
        virtual bool equals(const Object* const& other) const;


        /*!
         * オブジェクトのタイプを返します。
         */
        virtual Type* getType() const;


        /*!
         * オブジェクトのタイプ ID を返します。
         */
        virtual TypeID getTypeID() const;


        /*!
         * オブジェクトのハッシュ値を計算して返します。
         */
        virtual unsigned int getHashCode() const;


        /*!
         * オブジェクトの文字列表現を返します。
         */
        virtual String* toString() const;


        /*!
         *
         */
        void retain();


        /*!
         *
         */
        void release();


        /*!
         * 参照カウンタの値を返します。
         */
        unsigned int retainCount() const { return retain_count_; }

     protected:
        /*!
         * オブジェクトが破棄される前に、そのオブジェクトがリソースを開放し、その他のクリーンアップ操作を実行できるようにします。
         */
        virtual void finalize() {}

     private:
        unsigned int retain_count_;
        bool in_dynamic_;
    };


}


#endif  /* helium_Object_hxx */
// Local Variables:
//   coding: utf-8
// End:
